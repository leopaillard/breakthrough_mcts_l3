# Projet pour le module d'intelligence artificielle pour les étudiants de L3

Le but de ce projet est d'apprendre à explorer l'arbre de jeu via MCTS.

## Rappel

Pour rappel voici un lien vers les commandes git utiles pour ce module :
https://juliendehos.gitlab.io/posts/env/post20-git.html#forker-un-d%C3%A9p%C3%B4t-existant

## Etape 1 : Installation

- Forker ce dépôt.

Les paquets nécessaires sont `numpy`, `pillow` et `tkinter`.

## Etape 1 : Création d'un bot aléatoire

## Etape 2 : Création d'un bot Minmax ou Alpha-Beta

Pour cela, commencez par créer une heuristique. Pour pourrez associer des points à chaque pion en fonction de la distance qui le sépare de l'arrivée.

## Etape 3 : Création d'un bot MCTS vanilla

## Etape 4 : Création d'un bot UCT