import interface
import numpy as np
import random

class Joueur:
	def __init__(self, partie, couleur, opts={}):
		self.couleur = couleur
		self.couleurval = interface.couleur_to_couleurval(couleur)
		self.jeu = partie
		self.opts = opts

	def demande_coup(self):
		pass


class Humain(Joueur):

	def demande_coup(self):
		pass



class IA(Joueur):

	def __init__(self, partie, couleur, opts={}):
		super().__init__(partie,couleur,opts)
		self.temps_exe = 0
		self.nb_appels_jouer = 0
		


class Random(IA):
	
	def demande_coup(self):
		


class Minmax(IA):
	
	def demande_coup(self):
		(m, argm) = self.minmax(self.jeu.plateau, self.couleurval, 5)
		return argm
		
class AlphaBeta(IA):

	def demande_coup(self):
		pass #you know the drill


